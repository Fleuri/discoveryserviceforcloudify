To have scapy work, one has to set their python capabilities as follows:

sudo setcap cap_net_raw=eip /usr/bin/pythonX.X
sudo setcap cap_net_raw=eip /usr/bin/tcpdump

You can get the hostpool-service to use for testing by running:

"""
vagrant box add fleuri/hostpool-service-box
vagrant init fleuri/hostpool-service-box
