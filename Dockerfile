FROM python3.6
MAINTAINER lauri.suomalainen@klinik.fi

ENV PYTHONUNBUFFERED 1
ENV work_dir /app
RUN mkdir -p ${work_dir}
WORKDIR ${work_dir}

ADD requirements.txt ${work_dir}

RUN apt-get update
RUN pip install pip --upgrade && \
    pip install -r requirements.txt
                        
EXPOSE 5000

ADD . ${work_dir}

CMD python /app/manage.py runserver --host 0.0.0.0
