import paramiko

def run_command(command, spec_array, list_key, client):
    stdin, stdout, stderr = client.exec_command(command)
    line = stdout.readlines()
    spec_array[list_key] = line[0].strip('\n')


client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect('86.50.17.13', username='', password='')
spec_array = {}
command_list = dict({"cpus": "lscpu | awk '/^CPU\(s\):/{ print $2}'", 'ram': "free -m | awk '/Mem:/{ print $2}'"})

for list_key, command in command_list.items():
    run_command(command, spec_array, list_key, client)

hardware_specs = {'hardware_specs': spec_array}
print(hardware_specs)

client.close()
