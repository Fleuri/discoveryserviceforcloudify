import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:

    def __init__(self):
        pass

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):

    NETWORK_INTERFACE = "enp0s25"
    IP_RANGE = "192.168.150.1/24"
    REDIS_HOST = "localhost"
    REDIS_PORT = 6379
    REDIS_DB = 0

    PING_INTERVAL = 10  # In seconds
    PING_FAILURE_THRESHOLD = 3  # How many times ping can fail until a device is removed from memory

    HOST_POOL_SERVICE_ADDRESS = "http://127.0.0.1:5000" #Remember the protocol

    IGNORED_ADDRESSES = ["192.168.150.1"]


config = {
    'development': DevelopmentConfig,
    'default': DevelopmentConfig
}
