from flask import Flask
from flask_script import Manager, Server
from flask_bootstrap import Bootstrap
from app.arp_scanner.arpscanner import ArpScanner
from threading import Thread
from config import config


def start_arp_sniffer(scanner):
    with app.app_context():
        scanner.sniff_arps()


def start_arp_scan(scanner):
    with app.app_context():
        scanner.scan_arps()


def start_pinger(scanner):
    with app.app_context():
        scanner.ping_hosts()


class CustomServer(Server):
    def __call__(self, app, *args, **kwargs):
        with app.app_context():
            scanner = ArpScanner()
            start_up_thread = Thread(target=start_arp_scan, kwargs={'scanner': scanner})
            start_up_thread.start()
            start_up_thread.join()
           # exit(0) //This was used when running the start-up multiple times in a row
            sniffer_thread = Thread(target=start_arp_sniffer, kwargs={'scanner': scanner})
            sniffer_thread.start()
            pinger_thread = Thread(target=start_pinger, kwargs={'scanner': scanner})
            pinger_thread.start()
            return Server.__call__(self, app, *args, **kwargs)


app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config.from_object(config['development'])
config['development'].init_app(app)
app.config['SECRET_KEY'] = 'A very Hard secret'
manager = Manager(app)
manager.add_command('runserver', CustomServer())


@app.route('/', methods=['GET', 'POST'])
def index():
    pass


if __name__ == '__main__':
    manager.run()
