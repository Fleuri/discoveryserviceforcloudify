from flask import current_app
import requests
import json
import time

class RequestService:

    host_pool_service_address = None

    def __init__(self):
        self.host_pool_service_address = current_app.config['HOST_POOL_SERVICE_ADDRESS']
    """
    # Get hostpool id associated with a given ip address. None, if no such host
    """
    def get_id(self, ip_address: str, name: str = None):
        for host in self.get_hosts():
            if host['endpoint']['ip'] == ip_address or name is not None and host['name'] == name:
                return host['id']
        return None

    def get_hosts(self):
        return requests.get(self.host_pool_service_address + "/hosts").json()

    def get_a_host(self, id):
        return requests.get("{0}/host/{1}".format(self.host_pool_service_address, id)).json()

    """
    Host name is actually mac address. If host already exists, but the IP has changed, patch it
    """

    def register_a_new_host(self, ip_address: str, host_name: str) -> dict:
        stored_id = self.get_id(ip_address, host_name)
        if stored_id is None:
            data = {'hosts': []}
            host = dict()
            host['name'] = host_name
            host['os'] = 'linux'
            host['endpoint'] = {'ip': ip_address}
            host['endpoint']['protocol'] = 'ssh'
            host['endpoint']['port'] = 22
            host['credentials'] = {'password': 'admin', 'username': 'centos'}
            data['hosts'].append(host)
            print(json.dumps(data))
            r = requests.post(self.host_pool_service_address + "/hosts", json.dumps(data))
            message = "Added a new host to Host-pool Service!"
           # if ip_address == "192.168.150.4":
           #     found_time = time.time()
           #     output_file = open("slave_found_times.txt", "a")
           #     output_file.write(str(found_time) + '\n')
           #     message = "Slave-3 found"
           #     output_file.close()
            print(message)
            return {"json": r.json(), "message": message}
        else:
            return self.patch_a_host(stored_id, ip_address, host_name)

    def deregister_a_host(self, ip_address: str):
        id = self.get_id(ip_address)
        if id:
           # if ip_address == "192.168.150.4":
           #     found_time = time.time()
           #     output_file = open("slave_lost_times.txt", "a")
           #     output_file.write(str(found_time) + '\n')
           #     output_file.close()

            print("for id {0} we got ip address {1}".format(id, ip_address))
            return requests.delete("{0}/host/{1}".format(self.host_pool_service_address, id))

    """
        If we see a host with a changed ip or mac-address, we can patch the current one.
    """
    def patch_a_host(self, id, ip, name) -> dict:
        data = {}
        host = self.get_a_host(id)
        if host['endpoint']['ip'] != ip:
            data['endpoint'] = {'ip': ip}
        elif host['name'] != name:
            data['name'] = name
        else:
            return {"json": {}, "message": "Nothing was patched"}
        print(json.dumps(data))
        r = requests.patch("{0}/host/{1}".format(self.host_pool_service_address, id), json.dumps(data))
        found_time = time.time()
        output_file = open("slave_patched_times.txt", "a")
        output_file.write(str(found_time) + '\n')
        output_file.close()
        return {"json": r.json(), "message": "Patched an existing host"}

    def clear_hostpool_service_database(self):
        hosts = self.get_hosts()
        for host in hosts:
            self.deregister_a_host(host['endpoint']['ip'])
