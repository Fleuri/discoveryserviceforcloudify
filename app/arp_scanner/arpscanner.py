from flask import current_app
from scapy.all import srp, Ether, ARP, conf, sniff
import redis
from time import sleep, time
from app.request_service.request_service import RequestService
import threading

class ArpScanner:

    interface = None
    ip_range = None
    redis = None
    ping_failure_threshold = None
    ping_interval = None
    request_service = None

    def __init__(self):
        self.interface = current_app.config['NETWORK_INTERFACE']
        self.ip_range = current_app.config['IP_RANGE']
        self.redis = redis.Redis(host=current_app.config['REDIS_HOST'], port=current_app.config['REDIS_PORT'],
                                db=current_app.config['REDIS_DB'])
        self.ping_failure_threshold = current_app.config['PING_FAILURE_THRESHOLD']
        self.ping_interval = current_app.config['PING_INTERVAL']
        self.request_service = RequestService()

    def scan_arps(self):
        # output_file = open("Arp_scan_times.txt", "w")
        # for x in range(0, 30):
        #     start_time = time()
            print("Clearing databases")
            self.redis.flushdb()
            self.request_service.clear_hostpool_service_database()
            print("Executing start up ARP scan")
            conf.verb = 0
            ans, unans = srp(Ether(dst='ff:ff:ff:ff:ff:ff')/ARP(pdst=self.ip_range), timeout=2, iface=self.interface,
                             inter=0.001)
            for snd, rcv in ans:

                """ Store both IP addresses and times ping has failed using MAC address as a key. """
                if rcv.sprintf("%ARP.psrc%") not in current_app.config['IGNORED_ADDRESSES']:
                    self.request_service.register_a_new_host(rcv.sprintf("%ARP.psrc%"), rcv.sprintf("%ARP.hwsrc%"))
                    self.redis.hmset(rcv.sprintf("%ARP.hwsrc%"),
                                     {"ip_address": rcv.sprintf("%ARP.psrc%"), "ping_timeouts": 0})
                    print(rcv.sprintf("%Ether.src% - %ARP.psrc%"))
            print("Start up ARP scan done. Found " + str(self.redis.dbsize()) + " devices in the network.")
        #     end_time = time() - start_time
        #     output_file.write(str(end_time))
        #     output_file.write("\n")
        # output_file.close()

    def arp_monitor_callback(self, pkt):
        if ARP in pkt and pkt[ARP].op in (1,2):  # who-has or is-at
            if (pkt.sprintf("%ARP.psrc%") != '0.0.0.0' and
                    pkt.sprintf("%ARP.hwsrc%") != '00:00:00:00:00:00' and
                    pkt.sprintf("%ARP.hwsrc%") != 'ff:ff:ff:ff:ff:ff') and \
                    pkt.sprintf("%ARP.psrc%") not in current_app.config['IGNORED_ADDRESSES']:  # Ignore DHCP broadcasts and gratuitous ARP
                # If host doesn't exist, or host has changed IP address. Differentiating done in Request Service.
                if not self.redis.exists(pkt.sprintf("%ARP.hwsrc%")) or \
                       self.redis.hmget(pkt.sprintf("%ARP.hwsrc%"), 'ip_address')[0].decode('ascii') != pkt.sprintf("%ARP.psrc%"):

                    """ TODO: Request service methods should return a boolean indicating if the request was successful.
                    If request is unsuccessful, the device shouldn't be stored in redis  
                    """

                    threading.Thread(target=self.request_service.register_a_new_host, args=(pkt.sprintf("%ARP.psrc%"),
                                                                                            pkt.sprintf("%ARP.hwsrc%"))).start()
                self.redis.hmset(pkt.sprintf("%ARP.hwsrc%"),
                             {"ip_address": pkt.sprintf("%ARP.psrc%"), "ping_timeouts": 0})
            return pkt.sprintf("Caught ARP request from: %ARP.hwsrc% %ARP.psrc%")

    def sniff_arps(self):
       sniff(iface=self.interface, prn=self.arp_monitor_callback, filter="arp", store=0)
       #sniff(iface=self.interface, prn = lambda x: x.show(), filter="arp", store=0)

    def ping_hosts(self):
        while True:
            for key in self.redis.scan_iter("*"):
                print("Pinging " + str(key) + ": " + str(self.redis.hgetall(key)))
                host_ip = self.redis.hmget(key, 'ip_address')[0].decode("UTF-8")
                print(host_ip)
                print(type(host_ip))
                ping_timeouts = int(self.redis.hmget(key, 'ping_timeouts')[0])
                ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=host_ip), timeout=10, verbose=True, iface=self.interface )
                if ans:
                    print("Ping went through to " + host_ip)
                    self.redis.hmset(key, {'ping_timeouts': 0})
                else:
                    ping_timeouts = ping_timeouts + 1
                    print("Ping didn't go through to " + host_ip + ", current timeouts: " + str(ping_timeouts))
                    if ping_timeouts >= self.ping_failure_threshold:
                        print("Timeout threshold (" + str(self.ping_failure_threshold) + ") reached. Deregistering device.")
                        threading.Thread(target=self.request_service.deregister_a_host, args=(host_ip,)).start()  # TODO: Here too continue the loop if false.
                        self.redis.delete(key)
                    else:
                        self.redis.hmset(key, {'ping_timeouts': ping_timeouts})
            sleep(self.ping_interval)

